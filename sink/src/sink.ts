import { Client, Msg } from 'ts-nats';

import natsConnection from 'src/nats';
import redis from 'src/redis';

async function setupSink(): Promise<any> {
  const nats: Client = await natsConnection;

  nats.subscribe('user_1', (_: any, msg: Msg) => {
    const user: any = JSON.parse(msg.data);

    redis.setAsync(user.id, JSON.stringify(user));
  });
}

setupSink();
