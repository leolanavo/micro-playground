import { Client, connect } from 'ts-nats';

async function connectToNats(servers: string[]): Promise<Client> {
  return await connect({
    servers,
  });
}

export default connectToNats(['streaming']);
