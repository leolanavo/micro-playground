import Redis from 'redis';

import { promisifyAll } from 'bluebird';

const redisClient: any = promisifyAll(Redis.createClient({
  host: 'cache',
}));

export default redisClient;
