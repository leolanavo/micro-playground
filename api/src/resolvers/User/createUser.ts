import { Context } from 'koa';

import { User, UserInput, UserInputData } from 'src/resolvers/User/types';

async function createUser(args: UserInput, context: Context): Promise<User> {
  const { name, email }: UserInputData = args.user;

  const user: User = {
    id: '1',
    name,
    email,
  };

  context.nats.publish(`user_${user.id}`, JSON.stringify(user));

  return user;
}

export default createUser;
