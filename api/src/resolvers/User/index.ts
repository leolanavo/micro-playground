
import { Context } from 'koa';

import { User } from 'src/resolvers/User/types';

import getUser from 'src/resolvers/User/getUser';

import changeName from 'src/resolvers/User/changeName';
import createUser from 'src/resolvers/User/createUser';

export const UserQuery: any = {
  getUser(_: any, args: any, context: Context): Promise<User> {
    return getUser(args, context);
  },
};

export const UserMutation: any = {
  createUser(_: any, args: any, context: Context): Promise<User> {
    return createUser(args, context);
  },
  changeUserName(_: any, args: any, context: Context): Promise<User> {
    return changeName(args, context);
  },
};
